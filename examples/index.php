<?php

require __DIR__.'/../vendor/autoload.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

/*
 * The Engeni API Client
 * https://bitbucket.org/engeni-team/engeni-api-client
 */

use Engeni\ApiClient\Client;
use Engeni\ApiClient\Query;
use Engeni\ApiClient\Resource\Core\Account\Account;
use Engeni\ApiClient\Resource\Core\Country;
use Engeni\ApiClient\Resource\Core\Message;
use Engeni\ApiClient\Resource\Core\Partner;
use Engeni\ApiClient\Resource\Core\User\Profile as UserProfile;
use Engeni\ApiClient\Resource\Core\User\User;

// --------- CHANGE THIS VARIABLES
$coreApiUrl = 'https://api.test';
$authorizataionToken = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRkZWIwZWE0ZDIzOWY0YzQ3MmEwNDM0ZjgwOWQ2ZDdhNzg5NjU5MWIwOGI5Yjk5Nzk5OTIxZmRhNmZjMDg2YWRmZDM1NTUyY2FlMzEzYjNlIn0.eyJhdWQiOiIxIiwianRpIjoiZGRlYjBlYTRkMjM5ZjRjNDcyYTA0MzRmODA5ZDZkN2E3ODk2NTkxYjA4YjliOTk3OTk5MjFmZGE2ZmMwODZhZGZkMzU1NTJjYWUzMTNiM2UiLCJpYXQiOjE2MTM3ODc0MDEsIm5iZiI6MTYxMzc4NzQwMSwiZXhwIjoxNjEzODMwNjAxLCJzdWIiOiIxMiIsInNjb3BlcyI6W119.UNYgZc3J-KfXcJZneLQLl7_U2CkaVJRa5i4hr8lTckDaRLUpYKd_7aGIw1XPqPNzgQ7DPG6EgA7ljLFtSQ2tAvHdnqnz5RxepZU0PbSB-yO0u9d8IQYbBYODtQ-_YQzF5XwyDfLNJplL9u6Y1w4v3uFYwzliIMwUmclsr2PX5w77kyMC42E45Oqm90-Jjo5wEFarrSTFC4FAuJ8bWLf5hrfeYoxg4vkJSJl-uW2JtBCXTn7wpXwnnW0YiWa2tDAMto8gFSPDcD1b8lGgSvJuEJSznnil9T1FemX_cQsLovux33PMK1d95z9NY1V6_uLUlED1NYNdiSiDEuV_n3s2VnVIrOVF3KzaUh-k-lhYK4DsmEdOoTcc6zEH_yVZlXxhvWAcOWfZWVzRVpFpxjSzDZqUcpjyK9wiMs-0MhuZPdCobj8NoyvqKP41qan3RdH0m07iUR5qglALcJYDr7fv6-DkgT6d8z4s-HIF-pSduI6EBIJYnvqmvHsYYuows4849YAbw1eZkumULDu0OrYN65sUnHMcTkF0KA6jm0GWVj3kLVcO2g3GKqpCaakLMeKqKea7bYjd2-9BFWuZe9GNEqvBUvHdz1A2gwTTi-jphvy094m68DuqSAm6AhHPZO4vyCjdBUtsRfLvpft3IS2XB0ef7qmEsjTx0mOI5G2mniU';
$testAccountId = 1;
$testUserId = 12;
// ---------

$client = new Client([
    'base_uri' => $coreApiUrl,
    'debug' => true,
    'auth_token' => $authorizataionToken,
]);
$client->setAsGlobal();
echo '<pre>';

$query = (new Query)->setPath('/');
$response = (new Client)->get($query);
output(json_decode($response->getBody(), null, 512, JSON_THROW_ON_ERROR));

// title("UserLocation::fromUser($testUserId)...pluck('name', 'id')");
// $locations = Engeni\ApiClient\Resource\Core\Permission\UserLocation::fromUser($testUserId)
//     ->where('service_id', 'bdi')
//     ->whereIn('resource_name', ['products', 'prices'])
//     ->pluck('name', 'id');
// output($locations);

// / ------------------------------------------------------------------------------------

// title("Service::pluck('name', 'id')");
// $services = Engeni\ApiClient\Resource\Core\Service\Service::pluck('name', 'id');
// output($services);

// / ------------------------------------------------------------------------------------

// title('User::getByToken()');
// $user = User::getByToken($authorizataionToken, ['profile', 'language', 'accounts', 'userGroups', 'roles']);
// output($user->getAttributes());

// / ------------------------------------------------------------------------------------

// title('User::with(\'profile\')->find(3)');
// $user = User::with('profile')->find($testUserId);
// output($user);

// // / ------------------------------------------------------------------------------------

// title('User::saveSettings()');
// $user->saveSettings(array_filter([
//     'language' => 'en',
//     'current_account_id' => 1,
//     'current_location_id' => 1,
// ]));
// output($user);

// // / ------------------------------------------------------------------------------------

// // Provide a user id that is the same that the loged user.
// title("UserProfile::fromUser({$testUserId})");
// $user = UserProfile::fromUser($testUserId)->get();
// output($user);

// // / ------------------------------------------------------------------------------------

// title('Account::find($testAccountId)');
// $account = Account::find($testAccountId);
// output($account);

// // / ------------------------------------------------------------------------------------

// title("Account::select('id', 'name')->where('active', '1')->first():</h1>");
// $account = Account::select('id', 'name')
//     ->where('active', '1')
//     ->first()
// ;
// output($account);

// // / ------------------------------------------------------------------------------------

// title("Account::where('active', '1')->limit(2)->get():</h1>");
// $account = Account::where('active', '1')->limit(2)->get();
// output($account);

// // / ------------------------------------------------------------------------------------

// title('Country::all()');
// $countries = Country::all();
// output($countries);

// // / ------------------------------------------------------------------------------------

// title("Partner::where('country_id', 'ar')->get()");
// $partners = Partner::where('country_id', 'ar')->get();
// output($partners);

// // / ------------------------------------------------------------------------------------

// title('User::paginate()');
// $usersPaginator = User::paginate();
// output($usersPaginator->data);
// output(count($usersPaginator));
// foreach ($usersPaginator as $user) {
//     $usersIterated[] = $user->email;
// }
// output($usersIterated);
// $usersPaginatorExample = $usersPaginator;
// $usersPaginatorExample->data = '[ ... ]';
// output($usersPaginatorExample);

// // / ------------------------------------------------------------------------------------

// title("Partner::where('id', '1')->with('language')->get()");
// $partners = Partner::where('id', '1')
//     ->with('language')
//     ->get()
// ;
// output($partners);

// // / ------------------------------------------------------------------------------------

// title('Message::send()');
// echo 'uncomment the ->send() line';
// $message = new Message();
// $message->subscription = 'core'; // Optional, has to exist as a subscription
// $message->topic = 'api-client-test'; // Optional, up to 10 chars
// $message->account_id = 0; // Optional, must be an existing Account
// $message->payload = [
//     'from' => 'no-reply@notifications.engeni.com',
//     'fromName' => 'Engeni L.L.C.',
//     'to' => 'apardo@gmail.com',
//     'replyTo' => 'no-reply@notifications.engeni.com',
//     'replyToName' => 'Engeni S.A.',
//     'subject' => 'This is just a test',
//     'html_body' => '<p>this is a test message sent from the Engeni Api Client</p>',
// ];
// // $message->send(); // Uncoment this line to test messages

// // / ------------------------------------------------------------------------------------

// // RESET PASSWORDS :). Uncomment step 1, then comment step 1 and uncomment step 2.
// title('Reset passord step 1...');
// echo 'uncomment the Password::sendResetLink line';
// // output(Password::sendResetLink('admin@engeni.com'));
// title('Reset passord step 2...');
// echo 'uncomment the Password::reset line';
// // output(Password::reset('a1f85adea60ea6963a9b81d268f63b2cedc44ae652ea3575c381286296359546', '12345678', '12345678'));

// / ------------------------------------------------------------------------------------

/*

title("Business::where('account_id', 732)->first()");
$business = Business::where('account_id', 732)->first();
output($business);

title("Business::where('id', 770182)->first()->with('state')->with('city')->with('country')->first()");
$business = Business::where('id', 770182)
->with('state')
->with('city')
->with('categories')
->with('country')
->first();
output($business);
$business->removeCategory(4660);
$business->addCategory(4660);

title("business::create():</h1>");
$business = Business::create([
'name'=>'Empresa tessss',
'account_id' => 732,
'country_id' => 54
]);
output($business->getAttributes());

title("business::find('". $business->id ."'):</h1>");
$business = Business::find($business->id);
output($business->getAttributes());

title("business::fill()->save():</h1>");
$business = Business::find(770182);
$business->fill(['description'=>'asdasdasdas-'. 1]);
$business->save();
try {
$business->save();
output($business->getAttributes());
} catch (\Exception $e) {
echo $e->getMessage();
}

title('CreditCard::save() with Parent Resource');
$cc = new CreditCard;
$cc->fill(['type' => 'visa',
'account_id' => '732',
'nro' => '4111111111111111',
'expiration_month' => '12',
'expiration_year' => '2018',
'cs' => '123',
'name' => 'Test Testtt',
'address' => 'Avenida del Libertador 88',
'city' => 'Buenos Aires',
'state' => 'Buenos Aires',
'postcode' => '1638',
'country' => 'USA',
'chargeover_id' => '',
'default' => '10']);
try {
$cc->save();
output($cc->getAttributes());
} catch (\Exception $e) {
echo $e->getMessage();
}

title('CreditCard::find(732, $cc->id) with Parent Resource');
$cc = CreditCard::find($cc->id, 732);
output($cc);

title("CreditCardAttribute::find(10, $cc->id, 732) with Parent Resource<br><small>It's supposed to return not found.</small>");
try {
$cca = CreditCardAttribute::find(10, $cc->id, 732);
output($cca);
}
catch (\Exception $e) {
echo $e->getMessage();
}

title("CreditCardAttribute::fromParent($cc->id, 732)->where('default', '1')->get() with Parent Resource<br><small>It's supposed to return not found.</small>");
try {
$cca = CreditCardAttribute::fromParent($cc->id, 732)
->where('default', '1')
->get();
}
catch (\Exception $e) {
echo $e->getMessage();
}

title('CreditCard::create() with Parent Resource');
$cc = CreditCard::create(['type' => 'visa',
'account_id' => '732',
'nro' => '4111111111111111',
'expiration_month' => '12',
'expiration_year' => '2018',
'cs' => '123',
'name' => 'Test Testtt',
'address' => 'Avenida del Libertador 88',
'city' => 'Buenos Aires',
'state' => 'Buenos Aires',
'postcode' => '1638',
'country' => 'USA',
'chargeover_id' => '',
'default' => '0']);
output($cc->getAttributes());

title('CreditCard::star('. $cc->id .', 732)');
$starred = CreditCard::star($cc->id, 732);
output($starred);

title("CreditCard::fromAccount(732)->where('account_id', '732')->where('default', '1')->get() with Parent Resource");
$cc = CreditCard::fromAccount(732)
->where('account_id', '732')
->where('default', '1')
->get();
output($cc);

title("CreditCard::where(), pluck('type', 'nro') with Parent Resource");
$cc = CreditCard::fromAccount(732)
->where('default', '1')
->pluck('type', 'id');
output($cc);

 */

// // if this is not null, then message sent :)
// echo 'Message sent. Message ID: '. $message->id ."<br><br>\n";
//
function title($title)
{
    echo '<br><hr><h1>'.$title.'</h1><br>';
}
function output($data)
{
    echo '<div style="width: 100%; border: 1px; height: 250px; background-color: #aaa; overflow: scroll">';
    print_r($data);
    echo '</div><br>';
}
