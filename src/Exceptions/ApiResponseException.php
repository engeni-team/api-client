<?php

namespace Engeni\ApiClient\Exceptions;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ApiResponseException extends ApiClientException
{
    private array $messageBag = [
        'error' => [
            'code' => 0,
            'message' => '',
            'errors' => [],
        ],
        'debug' => '',
    ];

    /**
     * Class constructor.
     *
     * @param  string  $message  Error message
     * @param  int  $code  Error code
     */
    public function __construct($message = null, $code = 0)
    {
        $bag = json_decode((string) $message, true, JSON_UNESCAPED_SLASHES);

        if (json_last_error() == JSON_ERROR_NONE || ! is_array($bag) || ! isset($bag['error'])) {
            $this->messageBag = [
                'error' => [
                    'message' => $message,
                    'code' => $code,
                ],
            ];
        }

        $this->messageBag = array_merge($this->messageBag, $bag ?? []);

        parent::__construct(
            $this->messageBag['error']['message'],
            $this->messageBag['error']['code']
        );
    }

    public function getResponse($data = null)
    {
        return [
            'code' => $this->messageBag['error']['code'],
            'message' => $this->messageBag['error']['message'],
            'errors' => $this->messageBag['error']['errors'] ?? [],
            'data' => $data,
            'debug' => $this->messageBag['debug'] ?? [],
        ];
    }
}
