<?php

namespace Engeni\ApiClient\Exceptions;

use GuzzleHttp\Exception\ClientException;

/**
 * Standard exception thrown by Engeni Api Client
 */
class AuthorizationException extends \Exception
{
    final public const ERROR_CODE = 403;

    final public const ERROR_MESSAGE = 'Forbbiden';

    /**
     * Class constructor
     *
     * @param  ClientException|null  $error  Error message
     * @param  int|null  $code  Error code
     */
    public function __construct(?ClientException $error = null, ?int $code = null)
    {
        parent::__construct(
            $this->getErrorMessage($error) ?? self::ERROR_MESSAGE,
            $code ?: self::ERROR_CODE
        );
    }

    private function getErrorMessage(?ClientException $error = null): ?string
    {
        if ($error instanceof \GuzzleHttp\Exception\ClientException) {
            $bodyError = $error->getResponse()->getBody();
            $processedResponse = json_decode($bodyError, true, 512, JSON_THROW_ON_ERROR);

            return data_get($processedResponse, 'message', data_get($processedResponse, 'error.message'));
        }

        return null;
    }
}
