<?php

namespace Engeni\ApiClient\Exceptions;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ResourceNotFoundException extends ApiClientException
{
    public $resource;

    /**
     * The affected model IDs.
     *
     * @var array|int
     */
    protected $ids;

    private ?string $resourceClass = null;

    /**
     * Set the affected affected resource and instance ids.
     *
     * @param  string  $model
     * @return $this
     */
    public function setResource(string $resourceClass, array|int|null $ids = null)
    {
        $this->resourceClass = $resourceClass;

        if (! is_null($ids)) {
            $this->ids = is_array($ids) ? $ids : [$ids];
            $idsMsg = ':  '.implode(', ', $this->ids);
        } else {
            $this->ids = null;
            $idsMsg = '';
        }

        $this->message = sprintf("No query results for resource [{$this->resourceClass}%s]", $idsMsg);

        return $this;
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Get the affected Eloquent model IDs.
     */
    public function getIds(): array|int
    {
        return $this->ids;
    }
}
