<?php

namespace Engeni\ApiClient;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Exceptions\ApiResponseException;
use Exception;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class Client
{
    final public const VERSION = '5.x';

    final public const USER_AGENT_SUFFIX = 'engeni-api-client/';

    final public const API_BASE_URI = 'https://api.engeni.com';

    final public const API_BASE_PATH = '/';

    final public const APP_CONTEXT_ACCOUNT_HEADER = 'X-Account-Id';

    final public const APP_CONTEXT_LOCATION_HEADER = 'X-Location-Id';

    /**
     * @var array
     */
    protected $files = [];

    /**
     * @var bool
     */
    protected $isMultipart = false;

    /**
     * Scopes requested by the client.
     *
     * @var array
     */
    protected $requestedScopes = [];

    /**
     * The current globally used instance.
     *
     * @var object
     */
    protected static $instance;

    /**
     * @var GuzzleHttp\ClientInterface
     */
    private $http;

    /**
     * @var array access token
     */
    private $token;

    /**
     * @var array
     */
    private $config;

    private array $headers = [];

    /**
     * Construct the Engeni Client.
     * See http://docs.guzzlephp.org/en/stable/request-options.html for config $options.
     */
    public function __construct(array $config = [])
    {
        if (! self::$instance) {
            $this->config = array_merge(
                [
                    'version' => 2.0,
                    'application_name' => '',
                    'base_uri' => self::API_BASE_URI,
                    'base_path' => self::API_BASE_PATH,
                    'user_agent_suffix' => self::USER_AGENT_SUFFIX,
                    'client_id' => '',
                    'client_secret' => '',
                    'state' => null,
                    'auth_token' => null,
                    'timeout' => 10,
                    'retry' => [],
                    'debug' => false,
                    'verify' => false,
                    'jwt' => null,
                    'headers' => [],
                ],
                $config
            );
        } else {
            $this->config = self::$instance->getConfig();
        }
    }

    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Make this capsule instance available globally.
     */
    public function setAsGlobal(): Client
    {
        static::$instance = $this;

        return $this;
    }

    /**
     * Unregister singleton.
     */
    public static function forgetInstance()
    {
        static::$instance = null;
    }

    /**
     * Add a Header to the Request. Warning, it will overwrite previous headers.
     *
     * @param  array  $config
     */
    public function addHeader(string $header, string $value): Client
    {
        $this->headers[$header] = $value;

        return $this;
    }

    /**
     * Add a file attachment. Warning, this will force POST requests.
     *
     * @param  array  $config
     */
    public function addfile(string $name, string $filepath): Client
    {
        $this->files[] = [
            'name' => $name,
            'filepath' => $filepath,
        ];

        return $this;
    }

    /**
     * Executes http request.
     *
     * @param  \Engeni\ApiClient\Query  $query
     * @return Guzzle\Response
     */
    public function execute(mixed $verb, mixed $resource = '', mixed $params = []): GuzzleResponse
    {
        $http = $this->getHttpClient();

        try {
            $result = $http->request($verb, $resource, $params);
            if ($result->getStatusCode() >= 400) {
                throw new ApiResponseException($result->getBody()->getContents(), $result->getStatusCode());
            }
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            throw new \Exception($response->getBody()->getContents(), $e->getCode(), $e);
        }

        return $result;
    }

    /**
     * \Engeni\ApiClient\Query dependant method get.
     *
     * @return Guzzle\Response
     */
    public function get(Query $query): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = ['query' => $query->getPreparedQuery(), 'debug' => $this->config['debug']];

        return $this->execute('GET', $resource, $params);
    }

    /**
     * \Engeni\ApiClient\Query dependant method get.
     *
     * @return Guzzle\Response
     */
    public function put(Query $query, mixed $form_params = []): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = $this->getCompiledFormParams($form_params);
        $method = 'PUT';

        // Unforutnately, Laravel does not suport multipart form data with PUT method.
        // Se we have to add _method PUT to resolve it.
        if ($this->isMultipart) {
            $method = 'POST';
            $params['multipart'][] = [
                'name' => '_method',
                'contents' => 'PUT',
            ];
        }

        return $this->execute($method, $resource, $params);
    }

    /**
     * \Engeni\ApiClient\Query dependant method get.
     *
     * @return Guzzle\Response
     */
    public function post(Query $query, mixed $form_params = []): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = $this->getCompiledFormParams($form_params);

        return $this->execute('POST', $resource, $params);
    }

    /**
     * \Engeni\ApiClient\Query dependant method get.
     *
     * @return Guzzle\Response
     */
    public function delete(Query $query): GuzzleResponse
    {
        $resource = $query->getPath();
        $params = ['query' => $query->getPreparedQuery(), 'debug' => $this->config['debug']];

        return $this->execute('DELETE', $resource, $params);
    }

    /**
     * Set the Http Client object.
     *
     * @param  GuzzleHttp\ClientInterface  $http
     */
    public function setHttpClient(ClientInterface $http): Client
    {
        $this->http = $http;

        return $this;
    }

    /**
     * @return GuzzleHttp\ClientInterface implementation
     */
    public function getHttpClient()
    {
        if (! $this->http instanceof \Engeni\ApiClient\GuzzleHttp\ClientInterface) {
            $this->http = $this->createDefaultHttpClient();
        }

        return $this->http;
    }

    /** Get a string containing the version of the library.
     *
     * @return string
     */
    public function getLibraryVersion()
    {
        return self::VERSION;
    }

    /**
     * Compiles the form_params por the post-put method.
     *
     * Note:
     * multipart cannot be used with the form_params option.
     * You will need to use one or the other. Use form_params
     * for application/x-www-form-urlencoded requests,
     * and multipart for multipart/form-data requests.
     * This option cannot be used with body, form_params, or json.
     *
     * See https://docs.guzzlephp.org/en/stable/request-options.html#multipart for more info.
     */
    protected function getCompiledFormParams(array $form_params = []): array
    {
        $params = [];
        // If it has files, then it's multipart
        if ($this->files !== []) {
            $this->isMultipart = true;
            $params['multipart'] = [];

            foreach ($this->files as $file) {
                $params['multipart'][] = [
                    'name' => $file['name'],
                    'contents' => fopen($file['filepath'], 'r'),
                    'filename' => basename((string) $file['filepath']),
                ];
            }
        }
        // otherwise it's handled as form_params
        else {
            $this->isMultipart = false;
            $params['form_params'] = $form_params;
        }

        // debug_param
        $params['debug'] = $this->config['debug'];

        return $params;
    }

    /**
     * @return GuzzleHttp\ClientInterface implementation
     */
    protected function createDefaultHttpClient()
    {
        // Guzzle version control
        if (defined('\GuzzleHttp\ClientInterface::MAJOR_VERSION')) {
            // guzzle 6
            $version = \GuzzleHttp\ClientInterface::MAJOR_VERSION ?? null;
            if ($version < 7) {
                throw new Exception('Guzzle must be version 7 or newer');
            }
        } else {
            throw new Exception('Guzzle must be version 7 or newer');
        }
        // At this point Guzzle version is >= 7

        $options = [
            'http_errors' => false,
            'timeout' => $this->config['timeout'],
            'headers' => array_merge(array_merge([
                'User-Agent' => $this->config['application_name'].' '.self::USER_AGENT_SUFFIX.$this->getLibraryVersion(),
                'Accept' => 'application/json',
                'Authorization' => $this->config['auth_token'],
            ], $this->headers), $this->config['headers']),
            'base_uri' => $this->config['base_uri'],
            'base_path' => $this->config['base_path'],
            'verify' => $this->config['verify'],
        ];

        return new \GuzzleHttp\Client($options);
    }
}
