<?php

namespace Engeni\ApiClient\Abstracts;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Client;
use Engeni\ApiClient\Query;

abstract class ResourceAbstract implements \Stringable
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Indicates whether attributes are snake cased on arrays.
     *
     * @var bool
     */
    public static $snakeAttributes = true;

    /**
     * Indicates whether the resource exists or not.
     *
     * @var bool
     */
    public static $exists = false;

    /**
     * @var string
     */
    protected $resourceName;

    /**
     * The recource's parent. May be an array or a string with the parent resource class.
     * i.e.: '\Engeni\ApiClient\Resource\Account\Account' or ['Engeni\ApiClient\Resource\Account\Account', 'account_id'].
     *
     * @var mixed
     */
    protected $parentResource;

    /**
     * The recource's root Path. It is built when the Parent Resource IDs are sent through find, findOrNew, etc..
     *
     * @var mixed
     */
    protected $rootPath;

    /**
     * The primary key for the resource.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The recource's attributes.
     *
     * @var array
     */
    protected $attributes = [];

    private $parentResourceKeys = [];

    /**
     * @var \Engeni\ApiClient\Client
     */
    private $client;

    /**
     * The recource's Application Context [account_id, location_id].
     *
     * @var array
     */
    private $appContext = [];

    /**
     * Create a new Resource recource instance.
     */
    public function __construct(array $attributes = [])
    {
        $this->fill($attributes);
    }

    /**
     * Dynamically retrieve attributes on the resource.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the resource.
     *
     * @param  string  $key
     */
    public function __set($key, mixed $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Determine if an attribute or relation exists on the resource.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return ! is_null($this->getAttribute($key));
    }

    /**
     * Unset an attribute on the resource.
     *
     * @param  string  $key
     */
    public function __unset($key)
    {
        unset($this->attributes[$key], $this->relations[$key]);
    }

    /**
     * Handle dynamic method calls into the resource.
     * This is triggered when invoking inaccessible methods in an object context.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (in_array($method, ['increment', 'decrement'])) {
            return $this->{$method}(...$parameters);
        }

        // Case for "fromParent()". i.e CreditCard::fromAccount(732) or SomeClass::fromParent([732, 152])
        // is the Parent part of "fromParent" defined?
        if ($this->parentResource && preg_match('/from([A-Z]+\\w+)/', $method, $output) && ($output[1] == 'Parent' || $this->getParentResourceClassName(true) == $output[1])) {
            $this->setParentKeys($parameters);

            // $parameters[0] = parent_id or [parent_id]
            return $this->newQuery();
        }

        // else returns a Query method
        return $this->newQuery()->{$method}(...$parameters);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->{$method}(...$parameters);
    }

    /**
     * Convert the resource to its string representation.
     */
    public function __toString(): string
    {
        return $this->toJson();
    }

    /**
     * Get the resource name associated with the resource.
     */
    public function getResourceName(): string
    {
        if ($this->resourceName === null) {
            // TO-DO: return the resource name from a given class name
            exit('Inexistent $resourceName. Please define a resourceName.');
        }

        return $this->resourceName;
    }

    /**
     * Get an attribute from the resource.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if ($key === '' || $key === '0') {
            return null;
        }

        // If the attribute exists in the attribute array or has a "get" mutator we will
        // get the attribute's value. Otherwise, we will proceed as if the developers
        // are asking for a relationship's value. This covers both types of values.
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        return null;
    }

    /**
     * Set a given attribute on the resource.
     *
     * @param  string  $key
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function setAttribute($key, mixed $value)
    {
        $this->attributes[$key] = $value;

        return $this;
    }

    /**
     * Get all of the current attributes on the resource.
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * Fill the resource with an array of attributes.
     *
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function fill(array $attributes)
    {
        $this->exists = true; // -> TODO ver.
        foreach ($attributes as $key => $value) {
            if ($this->isRelationAttribute($key)) {
                $class = $this->getRelationClass($key);
                if (is_array($value)) {
                    $values = collect($value)->map(fn ($value) => new $class((array) $value));
                    $this->setAttribute($key, $values);
                } else {
                    $obj = new $class((array) $value);
                    $this->setAttribute($key, $obj);
                }
            } else {
                $this->setAttribute($key, $value);
            }
        }

        return $this;
    }

    public function forceFill(array $attributes)
    {
        return $this->fill($attributes);
    }

    public function isRelationAttribute($key)
    {
        return $this->getRelationClass($key);
    }

    public function getRelationClass($relationName)
    {
        return $this->relations && array_key_exists($relationName, $this->relations) ? $this->relations[$relationName] : null;
    }

    /**
     ******* PARENT RESOURCE METHODS *******.
     *
     * @param  mixed  $shortName
     */

    /**
     * Gets the parent class shortname.
     *
     * @param bool if setted then will return only the short parent short classname
     * @return null|string
     */
    public function getParentResourceClassName($shortName = false)
    {
        if ($this->parentResource) {
            $parentClassName = is_array($this->parentResource) ? $this->parentResource[0] : $this->parentResource;
            if ($shortName) {
                $rc = new \ReflectionClass($parentClassName);
                $parentClassName = $rc->getShortName();
            }

            return $parentClassName;
        }

        return null;
    }

    /**
     * Gets the parent Resource Key Name.
     *
     * @return null|string
     */
    public function getParentResourceKeyName()
    {
        if ($this->parentResource) {
            return is_array($this->parentResource) ? $this->parentResource[1] : strtolower((string) $this->getParentResourceClassName(true)).'_id';
        }

        return null;
    }

    /**
     * builds the root path of the resources.
     */
    public function getParentResourcePath(): string
    {
        // If the resource is not orphan ...
        if ($this->parentResource) {
            // First we get the $parentClassName and the $parentKeyName (we may need it later)
            // Then we build the parent class
            $parentClassName = $this->getParentResourceClassName();
            $parent = new $parentClassName;

            if (empty($this->parentResourceKeys)) {
                $parentKeyName = $this->getParentResourceKeyName();
                $parent->setKey($this->{$parentKeyName});
            } else {
                $parent->setKey($this->parentResourceKeys);
            }

            return $parent->getPath(); // <- this is recursive as whithin this method $parent->getParentResourcePath is called
        }

        return '';
    }

    /**
     * Sets parent resource keys.
     *
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function setParentKeys(mixed $parentKeys)
    {
        $this->parentResourceKeys = is_array($parentKeys) ? $parentKeys : [$parentKeys];

        return $this;
    }

    /**
     * Set whether IDs are incrementing.
     *
     * @param  bool  $value
     * @return $this
     */
    public function setIncrementing($value): bool
    {
        $this->incrementing = $value;

        return $this;
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     */
    public function getIncrementing(): bool
    {
        return $this->incrementing;
    }

    /**
     * Creates new resource within a diffent App Context.
     *
     * @param  array  $context  ['account_id',  'location_id']
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public static function withinContext(array $appContext)
    {
        $instance = new static;
        $instance->appContext = $appContext;

        return $instance;
    }

    /**
     ******* QUERY RELATED METHODS *******.
     */

    /**
     * Begin querying the resource.
     *
     * @return \Engeni\ApiClient\Query
     */
    public static function query()
    {
        return (new static)->newQuery();
    }

    /**
     * Get a new query for the resource.
     */
    public function newQuery(...$parameters): Query
    {
        if ($parameters !== []) {
            $this->setKey($parameters);
        }

        $client = $this->getClient();

        $query = (new Query)
            ->setClient($client)
            ->setResource($this)
            ->whereKeyName($this->getKeyName());

        if ($this->appContext !== []) {
            $query->withinContext($this->appContext);
        }

        return $query;
    }

    /**
     * Gets the computed resource path.
     *
     * @return string Resource complete URI
     */
    public function getPath(): string
    {
        return implode('/', array_filter([
            $this->parentResource ? null : $this->rootPath,
            $this->getParentResourcePath(),
            $this->getResourceName(),
            $this->getKey(),
        ]));
    }

    /**
     * Update the resource in the database.
     */
    public function update(array $attributes = [], array $options = []): bool
    {
        if (! $this->exists) {
            return false;
        }

        return $this->fill($attributes)->save($options);
    }

    /**
     * Update the resource in the database.
     *
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public static function create(array $attributes = [], array $options = [])
    {
        $resource = new static;
        $resource->exists = true;
        $resource->fill($attributes)->save($options);

        return $resource;
    }

    /**
     * Save the resource to the database.
     */
    public function save(array $options = []): bool
    {
        $this->getClient();
        $query = $this->newQuery();

        if ($this->getIncrementing() && $this->getKey()) {
            $response = $this->client->put($query, $this->getAttributes());
            if ($response->getStatusCode() == 200) {
                return true;
            }
        } else {
            $response = $this->client->post($query, $this->getAttributes());
            if ($response->getStatusCode() == 201) {
                // We try to get the inserted ID. And, this is the expected response:
                // {
                //     "code": 201,
                //     "status": "success",
                //     "data": {
                //        ... And object with the model attributes, thus, the Key Name of the model is in here.
                //     }
                // }
                $body = json_decode($response->getBody(), null, 512, JSON_THROW_ON_ERROR);
                $keyName = $this->getKeyName();
                if (isset($body->data->{$keyName})) {
                    $this->setAttribute($keyName, $body->data->{$keyName});
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Destroy the models for the given IDs.
     *
     * @param  array|int  $ids
     */
    public static function destroy($ids): int
    {
        // We'll initialize a count here so we will return the total number of deletes
        // for the operation. The developers can then check this number as a boolean
        // type value or get this total count of records deleted for logging, etc.
        $count = 0;

        $ids = is_array($ids) ? $ids : func_get_args();

        // We will actually pull the models from the database table and call delete on
        // each of them individually so that their events get fired properly with a
        // correct set of attributes in case the developers wants to check these.
        $key = ($instance = new static)->getKeyName();

        foreach ($instance->where($key, $ids)->first() as $resource) {
            if ($resource->delete()) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Delete the resource from the database.
     *
     * @return null|bool
     *
     * @throws \Exception
     */
    public function delete()
    {
        if (is_null($this->getKeyName())) {
            throw new Exception('No primary key defined on recource.');
        }

        // If the resource doesn't exist, there is nothing to delete so we'll just return
        // immediately and not do anything else. Otherwise, we will continue with a
        // deletion process on the resource, firing the proper events, and so forth.
        if (! $this->exists) {
            return null;
        }

        $this->getClient();
        $query = $this->newQuery();
        $this->client->delete($query);

        return true;
    }

    /**
     * Perform an action using the resource.
     *
     * @param  array  $parameters
     * @return null|bool
     *
     * @throws \Exception
     */
    public static function do(string $subResourceName, mixed $options = '', ...$parameters)
    {
        // Refer to https://docs.guzzlephp.org/en/stable/request-options.html?highlight=form_params#form-params
        $formParams = [];

        if (is_array($options)) {
            if (isset($options['form_params'])) {
                $formParams = $options['form_params'];
            }
            $verb = $options['verb'] ?? 'GET';
        } else {
            $verb = $options ?? 'GET';
        }

        $instance = new static;
        $query = $instance->newQuery(...$parameters)
            ->setPath($instance->getPath().'/'.$subResourceName);

        $response = $instance->getClient()
            ->{$verb}($query, $formParams);

        return $response->getStatusCode() == 200;
    }

    /**
     ****** OPERATION RELATED METHODS ******.
     */

    /**
     * Convert the resource instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * Convert the resource instance to JSON.
     *
     * @param  int  $options
     * @return string
     *
     * @throws Exception
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \ErrorException('Error executing toJson');
        }

        return $json;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Get the database connection for the resource.
     */
    public function getClient(): Client
    {
        if (! $this->client) {
            $this->setClient(new Client);
        }

        return $this->client;
    }

    /**
     * Set the connection associated with the resource.
     *
     * @param  string  $name
     * @return $this
     */
    public function setClient(mixed $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get the primary key for the resource.
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->primaryKey;
    }

    /**
     * Set the primary key for the resource.
     *
     * @param  string  $key
     * @return $this
     */
    public function setKeyName($key = null)
    {
        if ($key) {
            $this->primaryKey = $key;
        }

        return $this;
    }

    /**
     * Get the value of the resource's primary key.
     *
     * @param  string  $id
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function setKey(mixed $keys)
    {
        if (is_array($keys)) {
            // The first one is always the resource key
            $id = array_shift($keys);
            $this->setParentKeys($keys);
        } else {
            $id = $keys;
        }

        $primaryKey = $this->getKeyName();
        $this->{$primaryKey} = $id;

        return $this;
    }

    /**
     * Get the value of the resource's primary key.
     *
     * @return mixed
     */
    public function getKey()
    {
        return $this->getAttribute($this->getKeyName());
    }

    /**
     * Get the number of recources to return per page.
     *
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * Set the number of recources to return per page.
     *
     * @param  int  $perPage
     * @return $this
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }
}
