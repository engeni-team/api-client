<?php

namespace Engeni\ApiClient\Abstracts;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

abstract class WebhookResourceAbstract extends ResourceAbstract
{
    protected $resourceName = 'webhooks';

    public static function process(string $type, array $payload = [])
    {
        if ($type === '') {
            throw new \Exception('The webhook type cannot be empty.');
        }

        return self::do(
            $type,
            [
                'verb' => 'POST',
                'form_params' => $payload,
            ]
        );
    }

    public static function ThrowNotImplemented()
    {
        trigger_error('Webhooks are not implemented in this service.', E_USER_ERROR);
    }
}
