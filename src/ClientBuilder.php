<?php

namespace Engeni\ApiClient;

class ClientBuilder
{
    public function setCLIClient(?int $userId = null, ?int $locationId = null, ?int $accountId = null)
    {
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $response = $http->request('POST', config('engeni.api-client.base_uri').'/oauth/token', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'client_id' => config('engeni.engeni_user_client_id'),
                'client_secret' => config('engeni.engeni_user_client_secret'),
                'grant_type' => 'client_credentials',
            ],
        ]);

        $token = json_decode((string) $response->getBody(), true, 512, JSON_THROW_ON_ERROR)['access_token'];

        Client::forgetInstance();

        $headers = array_filter(['X-User-ID' => $userId, 'X-Account-ID' => $accountId, 'X-Location-ID' => $locationId]);

        $client = new Client([
            'user_class' => config('engeni.api-client.user_class', false),
            'base_uri' => config('engeni.api-client.base_uri'),
            'debug' => config('engeni.api-client.debug', false),
            'timeout' => config('engeni.api-client.timeout', 30),
            'auth_token' => $token,
            'headers' => $headers,
        ]);

        $client->setAsGlobal();
    }
}
