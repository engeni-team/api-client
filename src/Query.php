<?php

namespace Engeni\ApiClient;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Abstracts\ResourceAbstract;
use Engeni\ApiClient\Exceptions\ApiResponseException;
use Engeni\ApiClient\Exceptions\ResourceNotFoundException;

class Query implements \Stringable
{
    /**
     * The path where the query is executed.
     */
    private string $path;

    private ResourceAbstract $resource;

    private array $with = [];

    private array $filters = [];

    private array $sorting = [];

    private $columns = [];

    private bool $pagination = false;

    private int $limit;

    private string $whereKeyName;

    private array $forPage = [];

    /**
     * @var Engeni\ApiClient\Client
     */
    private $client;

    /**
     * Create a new Resource recource instance.
     *
     * @param  array  $attributes
     */
    public function __construct()
    {
        // app is a Laravel/lumen function
        $this->client = function_exists('app') ? app(Client::class) : Client::class;
    }

    /**
     * Convert the resource to its string representation.
     */
    public function __toString(): string
    {
        return http_build_query($this->getPreparedQuery());
    }

    /**
     * Handle dynamic method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        // $className = static::class;

        // throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
    }

    /**
     * Sets the client to execute the query.
     *
     * @return \Engeni\ApiClient\Query
     */
    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Creates new resource within a diffent App Context.
     *
     * @param  array  $context  ['account_id',  'location_id']
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function withinContext(array $appContext)
    {
        if (isset($appContext['account_id'])) {
            $this->client->addHeader(Client::APP_CONTEXT_ACCOUNT_HEADER, $appContext['account_id']);
        }

        if (isset($appContext['location_id'])) {
            $this->client->addHeader(Client::APP_CONTEXT_LOCATION_HEADER, $appContext['location_id']);
        }

        return $this;
    }

    /**
     * Sets the path to execute the query.
     *
     * @param  string  $path
     * @return Engeni\ApiClient\Query
     */
    public function setPath($path = '/')
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Sets the path to execute the query.
     *
     * @param  string  $path
     * @return Engeni\ApiClient\Query
     */
    public function setResource(mixed $resource)
    {
        $this->resource = $resource;

        return $this;
    }

    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Sets the path to execute the query.
     *
     * @param  string  $path
     * @return Engeni\ApiClient\Query
     */
    public function getPath()
    {
        return $this->path ?? $this->resource->getPath();
    }

    /**
     * Resource key name (i.e. id).
     *
     * @param  string  $keyName
     * @return Engeni\ApiClient\Query
     */
    public function whereKeyName($keyName)
    {
        $this->whereKeyName = $keyName;

        return $this;
    }

    /**
     * @param  string  $field
     * @param  string  $value
     * @return Engeni\ApiClient\Query
     */
    public function select(...$columns)
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @param  string  $field
     * @return Engeni\ApiClient\Query
     */
    public function where($field, mixed $value)
    {
        $processedValue = $this->processParameterValue($value);

        if ((string) $field === $this->whereKeyName) {
            $this->resource->setKey($processedValue);
        } else {
            $this->filters[$field] = $processedValue;
        }

        return $this;
    }

    /**
     * @param  string  $field
     * @return Engeni\ApiClient\Query
     */
    public function whereIn($field, array $values = [])
    {
        if (is_array($values)) {
            foreach ($values as $value) {
                $this->filters[$field][] = $this->processParameterValue($value);
            }
        }

        return $this;
    }

    /**
     * @param  string  $limit
     * @return Engeni\ApiClient\Query
     */
    public function limit($limit = 20)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Resource relations (i.e. 'language' where language is a relation of landing landing->language).
     *
     * @param  string  $relations
     * @return Engeni\ApiClient\Query
     */
    public function with($relations)
    {
        $this->with[] = $relations;

        return $this;
    }

    /**
     * @param  string  $field
     * @param  string  $order
     * @return Engeni\ApiClient\Query
     */
    public function orderBy($field, $order = 'ASC')
    {
        $this->sorting[$field] = $order;

        return $this;
    }

    /**
     * Prepared query to send to the http client
     */
    public function getPreparedQuery(): array
    {
        return array_filter(array_merge(
            $this->forPage,
            $this->getFilters(),
            [
                'fields' => $this->prepareColumns(),
                'embed' => $this->prepareWhith(),
                'sort' => $this->prepareSorting(),
                'limit' => $this->limit ?? null,
                'no_pagination' => ($this->pagination != true) ? 1 : 0,
            ]
        ));
    }

    /**
     * Execute the query as a "select" statement.
     */
    public function getFromApi()
    {
        try {
            $response = $this->client->get($this);
        } catch (ApiResponseException $e) {
            if ($e->getCode() == 404) {
                return null;
            }
            throw $e;
        }

        // print_r($response);
        if ($response->getStatusCode() == '200') {
            return json_decode((string) $response->getBody()->getContents(), null, 512, JSON_THROW_ON_ERROR);
        }
    }

    /**
     * Execute the query as a "select" statement.
     */
    public function get(): ?array
    {
        $this->pagination = false;
        $body = $this->getFromApi();

        if (! is_null($body)) {
            return $this->getCollection($body->data);
        }

        return null;
    }

    /**
     * Alias from get().
     */
    public function all(): ?array
    {
        return $this->get();
    }

    /**
     * Set the limit and offset for a given page.
     * page and per_page are strings that cannot be changed as they belong to the Engeni API definition.
     *
     * @param  int  $page
     * @param  int  $perPage
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function forPage($page, $perPage = 15)
    {
        $this->forPage = [
            'page' => $page,
            'per_page' => $perPage,
        ];
    }

    /**
     * Paginate the given query.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @return Engeni\ApiClient\Paginator
     *
     * @throws \InvalidArgumentException
     */
    public function paginate($perPage = null, $columns = [], $pageName = 'page', ?int $page = null)
    {
        $this->pagination = true;

        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $perPage = (int) $perPage ?: $this->resource->getPerPage();

        $this->columns = $columns ?: $this->columns;

        $this->forPage($page, $perPage);

        $body = $this->getFromApi();

        if ($body != null) {
            return new Paginator((array) $body->data);
        }

        return null;
    }

    /**
     * Execute the query as a "first" statement.
     *
     * @return null|\Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function first()
    {
        $collection = $this->limit(1)->get();
        if (isset($collection[0])) {
            $this->resource->fill((array) $collection[0]);

            return $this->resource;
        }

        return null;
    }

    /**
     * Execute the query as a "first" statement or throw an exception.
     *
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     *
     * @throws \ErrorException
     */
    public function firstOrFail()
    {
        $result = $this->first();
        if (is_null($result)) {
            throw (new ResourceNotFoundException)->setResource($this->resource::class);
        }

        return $result;
    }

    /**
     * Find a resource by its primary key (and parent Key | optional).
     *
     * @param  array  $columns
     * @return null|\Engeni\ApiClient\Resource\ResourceAbastract
     */
    public function find(mixed $id)
    {
        $processedId = $this->processParameterValue($id);

        return $this->where($this->whereKeyName, $processedId)
            ->first();
    }

    /**
     * Find a resource by its primary key or throw an exception.
     *
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     *
     * @throws \ErrorException
     */
    public function findOrFail(mixed $id)
    {
        $processedId = $this->processParameterValue($id);

        $result = $this->find($processedId);
        if (is_null($result)) {
            throw (new ResourceNotFoundException)->setResource($this->resource::class, $processedId);
        }

        return $result;
    }

    /**
     * Find a resource by its primary key or create a new one.
     *
     * @param  mixed  $id  | $parent_id, $id
     * @return \Engeni\ApiClient\Resource\ResourceAbastract
     *
     * @throws \ErrorException
     */
    public function findOrNew(...$parameters)
    {
        try {
            $result = $this->find(...$parameters);
            if (! is_null($result)) {
                return $result;
            }
        }
        // Not found. We don't do anything as we need to continue to execute the following code
        catch (\Exception) {
        }

        // If not found, create new instance...
        return $this->resource->setKey($parameters);
    }

    /**
     * Get an array with the values of a given column.
     *
     * @param  string  $column
     * @return []
     */
    public function pluck($column, ?string $key = null)
    {
        if (($collection = $this->get()) != null) {
            $results = [];

            foreach ($collection as $item) {
                // If the key is "null", we will just append the value to the array and keep
                // looping. Otherwise we will key the array using the value of the key we
                // received from the developer. Then we'll return the final array form.
                if (is_null($key)) {
                    $results[] = $item->{$column};
                } else {
                    $results[$item->{$key}] = $item->{$column};
                }
            }

            return $results;
        }

        return null;
    }

    // Function that handles different parameter types
    private function processParameterValue(mixed $value = null): mixed
    {
        return match (true) {
            is_null($value) => null,
            is_string($value) => $value,
            is_array($value) => throw new \Exception('Error processing parameter value: array type not supported.'),
            is_numeric($value) => $value,
            is_bool($value) => $value ? 1 : 0,
            $value instanceof \UnitEnum => $value->value,
            default => 'Invalid parameter value',
        };
    }

    private function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return string Prepared string to send to http client
     */
    private function prepareColumns(): string
    {
        return implode(',', $this->columns);
    }

    /**
     * @return string Prepared string to send to http client
     */
    private function prepareWhith(): string
    {
        return implode(',', $this->with);
    }

    /**
     * @return string Prepared string to send to http client
     */
    private function prepareSorting(): string
    {
        $preparedSorting = [];
        foreach ($this->sorting as $field => $order) {
            $preparedSorting[] = (strtoupper((string) $order) == 'ASC' ? '' : '-').$field;
        }

        return implode(',', $preparedSorting);
    }

    private function getCollection($data): array
    {
        $collection = [];
        if (is_array($data)) {
            $collection = $data;
        } else {
            $collection[] = $data;
        }

        return $collection;
    }
}
