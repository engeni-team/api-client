<?php

namespace Engeni\ApiClient\Resource\Atlas;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Exception;

class Site extends BaseResource
{
    protected $resourceName = 'sites';

    /**
     * @throws Exception
     */
    public function publish(array $attributes = []): bool
    {
        $resource = "{$this->id}/publish";

        return self::do($resource, [
            'form_params' => $attributes,
            'verb' => 'POST',
        ]);
    }

    /**
     * @throws Exception
     */
    public function republish(array $attributes = []): bool
    {
        return $this->publish($attributes);
    }

    public function suspend(array $attributes = []): bool
    {
        $resource = "{$this->id}/suspend";

        return self::do($resource, [
            'form_params' => $attributes,
            'verb' => 'POST',
        ]);
    }

    public function cancel(array $attributes = []): bool
    {
        $resource = "{$this->id}/destroy";

        return self::do($resource, [
            'form_params' => $attributes,
            'verb' => 'POST',
        ]);
    }
}
