<?php

namespace Engeni\ApiClient\Resource\Landkit;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Account extends BaseResource
{
    protected $resourceName = 'accounts';

    public function getStats(array $params = [])
    {
        if (! $this->id) {
            throw new Exception('Please load an id attribute to be able to get stats.');
        }

        $data = $this->getData('/stats?'.http_build_query($params));
        $data_contents = $data->getBody()->getContents();

        return json_decode((string) $data_contents, null, 512, JSON_THROW_ON_ERROR)->data;
    }
}
