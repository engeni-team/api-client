<?php

namespace Engeni\ApiClient\Resource\Landkit;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Landing extends BaseResource
{
    protected $resourceName = 'landings';

    public function setup($params)
    {
        return self::do('setup', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function suspend()
    {
        $query = self::newQuery()->setPath($this->getPath().'/suspend');

        return $this->getClient()->post($query);
    }

    public function activate()
    {
        $query = self::newQuery()->setPath($this->getPath().'/activate');

        return $this->getClient()->post($query);
    }

    public function cancel()
    {
        $query = self::newQuery()->setPath($this->getPath().'/cancel');

        return $this->getClient()->post($query);
    }

    public function generateMonitor(array $params = [])
    {
        $this->validate();
        $cycle = $params['cycle'] ?? null;
        $query = self::newQuery()->setPath($this->getPath().'/monitors/generate');
        if ($cycle) {
            $query->setPath($query->getPath().'?cycle='.$cycle);
        }

        return $this->getClient()->post($query);
    }

    public function lastLights()
    {
        $query = self::newQuery()->setPath($this->getPath().'/last_lights');

        return $this->getResource($query);
    }

    public function monitors(array $params = [])
    {
        $limit = $params['limit'] ?? null;
        $createdAt = $params['created_at'] ?? null;
        $query = self::newQuery()->setPath($this->getPath().'/monitors');
        if ($limit) {
            $query->limit($limit);
        }
        if ($createdAt) {
            $query->where('created_at', $createdAt);
        }

        return $this->getResource($query);
    }

    public function statistics(array $params = [])
    {
        $limit = $params['limit'] ?? null;

        $query = self::newQuery()->setPath($this->getPath().'/statistics');
        if ($limit) {
            $query->limit($limit);
        }

        return $this->getResource($query);
    }

    public function widget(array $params = [])
    {
        $this->validate();
        $query = self::newQuery()->setPath($this->getPath().'/widget');

        if (isset($params['from'], $params['to'])) {
            $query->where('from', $params['from'])->where('to', $params['to']);
        }
        if (isset($params['embed'])) {
            $query->with($params['embed']);
        }

        return $this->getResource($query);
    }

    public function sem()
    {
        $this->validate();
        $query = self::newQuery()->setPath($this->getPath().'/sem');

        return $this->getResource($query);
    }

    protected function validate()
    {
        if (! $this->id) {
            throw new Exception('It is not possible to get a widget from an invalid Landing.');
        }
    }

    protected function getResource($query)
    {
        $resource = $this->getClient()->get($query);
        $contentData = $resource->getBody()->getContents();

        return json_decode($contentData, null, 512, JSON_THROW_ON_ERROR)->data;
    }
}
