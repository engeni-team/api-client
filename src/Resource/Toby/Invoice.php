<?php

namespace Engeni\ApiClient\Resource\Toby;

use Exception;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Invoice extends BaseResource
{
    protected $resourceName = 'invoices';

    public function void()
    {
        if (! $this->id) {
            throw new Exception('It is not possible to void from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/void');

        return $this->getClient()->post($query);
    }

    public function charge(bool $asyncMode = false)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to charge from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/charge');

        return $this->getClient()->post($query, [
            'async_mode' => $asyncMode,
        ]);
    }

    public function retryCharge(bool $asyncMode = false, ?int $paymentMethodId = null)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to charge from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/retry-charge');

        return $this->getClient()->post($query, [
            'async_mode' => $asyncMode,
            'payment_method_id' => $paymentMethodId,
        ]);
    }

    public function retryInvoice(bool $asyncMode = false)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to invoice from an invalid id.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/retry-invoice');

        return $this->getClient()->post($query, ['async_mode' => $asyncMode]);
    }

    public function invoice(bool $asyncMode = false)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to invoice from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/invoice');

        return $this->getClient()->post($query, [
            'async_mode' => $asyncMode,
        ]);
    }

    public function chargeThenInvoice(bool $asyncMode = false)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to chargeThenInvoice from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/charge-then-invoice');

        return $this->getClient()->post($query, [
            'async_mode' => $asyncMode,
        ]);
    }

    public function invoiceThenCharge(bool $asyncMode = false)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to invoiceThenCharge from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/invoice-then-charge');

        return $this->getClient()->post($query, [
            'async_mode' => $asyncMode,
        ]);
    }

    public function getPDF(bool $asyncMode = false)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to invoiceThenCharge from an invalid invoice.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/pdf');

        return $this->getClient()->get($query);
    }
}
