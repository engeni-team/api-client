<?php

namespace Engeni\ApiClient\Resource\Toby;

use Exception;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ChargeTransaction extends BaseResource
{
    protected $resourceName = 'charge-transactions';

    protected $parentResource = \Engeni\ApiClient\Resource\Toby\Invoice::class;

    public function externalAction(array $data)
    {
        if (! $this->id) {
            throw new Exception('It is not possible to external action from an invalid charge transaction.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/external-action');

        return $this->getClient()->post($query, $data);
    }
}
