<?php

namespace Engeni\ApiClient\Resource\ChurnPredictor;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Abstracts\ResourceAbstract;

class Prediction extends ResourceAbstract
{
    final public const RESOURCE_PATH = 'https://sq64yq3iw3.execute-api.us-east-1.amazonaws.com/prod/churn/predictions/';

    public function getPredictions($params)
    {
        $data = $this->getClient()->execute('GET', self::RESOURCE_PATH, ['timeout' => 10, 'query' => $params]);
        $data_contents = $data->getBody()->getContents();

        return json_decode($data_contents, null, 512, JSON_THROW_ON_ERROR);
    }
}
