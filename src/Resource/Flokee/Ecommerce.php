<?php

namespace Engeni\ApiClient\Resource\Flokee;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Ecommerce extends BaseResource
{
    protected $resourceName = 'ecommerces';

    public function setup($params)
    {
        return self::do('setup', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function authorize($params)
    {
        return self::do($this->getKey().'/authorize', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function widget(array $options = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/widget');

        if (isset($options['range'])) {
            $query->where('range', $options['range']);
        }

        return $this->getResource($query);
    }

    public function getMonitorsStats(array $options = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/stats/monitors');

        if (isset($options['range'])) {
            $query->where('range', $options['range']);
        }

        return $this->getResource($query);
    }

    public function getProductsStats(array $options = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/stats/products');

        if (isset($options['range'])) {
            $query->where('range', $options['range']);
        }

        return $this->getResource($query);
    }

    public function getSales(array $options = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/sales');

        if (isset($options['range'])) {
            $query->where('range', $options['range']);
        }

        if (isset($options['page'])) {
            $query->where('page', $options['page']);
        }

        if (isset($options['pre_activation_sales'])) {
            $query->where('pre_activation_sales', $options['pre_activation_sales']);
        }

        if (isset($options['completed'])) {
            $query->where('completed', $options['completed']);
        }

        return $this->getResource($query);
    }

    public function getSummaryStats(array $options = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/stats/summary');

        if (isset($options['range'])) {
            $query->where('range', $options['range']);
        }

        return $this->getResource($query);
    }

    public function getStatus()
    {
        $query = self::newQuery()->setPath($this->getPath().'/status');
        $resource = $this->getClient()->get($query);

        return json_decode($resource->getBody()->getContents(), null, 512, JSON_THROW_ON_ERROR)?->data;
    }

    public function getProducts(array $options = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/products');

        if (isset($options['limit'])) {
            $query->limit($options['limit']);
        }

        return $this->getResource($query);
    }

    public function getContentOfGoogleSiteVerificationFile()
    {
        $query = self::newQuery()->setPath($this->getPath().'/google/download-site-verification-file');
        $resource = $this->getClient()->get($query);

        return $resource->getBody()->getContents();
    }

    public function getContentOfGoogleSiteVerificationMetaTag()
    {
        $query = self::newQuery()->setPath($this->getPath().'/google/get-verification-meta-tag');
        $resource = $this->getClient()->get($query);

        return json_decode($resource->getBody()->getContents(), null, 512, JSON_THROW_ON_ERROR);
    }

    public function getShopifyCustomPixel()
    {
        $query = self::newQuery()->setPath($this->getPath().'/google/get-shopify-custom-pixel');
        $resource = $this->getClient()->get($query);

        return json_decode($resource->getBody()->getContents(), null, 512, JSON_THROW_ON_ERROR);
    }

    public function syncProducts()
    {
        return self::do($this->getKey().'/products/sync', [
            'verb' => 'POST',
        ]);
    }

    public function syncSales()
    {
        return self::do($this->getKey().'/sales/sync', [
            'verb' => 'POST',
        ]);
    }

    public function googleCheckVerificationFile()
    {
        return self::do($this->getKey().'/google/check-verification', [
            'verb' => 'POST',
        ]);
    }

    public function googleClaimWebsite()
    {
        return self::do($this->getKey().'/google/claim-website', [
            'verb' => 'POST',
        ]);
    }

    public function notifyNewCycleCreatedEvent(array $params)
    {
        return self::do($this->getKey().'/events/new-cycle-created', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function executeStatusTransition(string $status)
    {
        return self::do($this->getKey().'/status/transition/'.$status, [
            'verb' => 'POST',
        ]);
    }

    public function updateSettings(array $params)
    {
        return self::do($this->getKey().'/settings', [
            'verb' => 'PUT',
            'form_params' => $params,
        ]);
    }
}
