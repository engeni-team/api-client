<?php

namespace Engeni\ApiClient\Resource\CXM\Product;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Landkit extends AbstractProductResource
{
    protected $resourceName = 'landkits';

    public function getSettings()
    {
        $query = self::newQuery()->setPath($this->getPath().'/settings');

        return $this->getResource($query);
    }

    protected function getResource($query)
    {
        $resource = $this->getClient()->get($query);
        $contentData = $resource->getBody()->getContents();

        return json_decode($contentData, null, 512, JSON_THROW_ON_ERROR)->data;
    }
}
