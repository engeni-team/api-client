<?php

namespace Engeni\ApiClient\Resource\CXM;

class Notification extends BaseResource
{
    protected $resourceName = 'notifications';

    public static function send($users, $params): ?bool
    {
        return self::do('send', [
            'verb' => 'POST',
            'users' => $users,
            'form_params' => [
                'users' => $users,
                'topic' => $params['topic'],
                'title' => $params['title'],
                'body' => $params['body'],
                'data' => $params['data'],
                'via' => $params['via'],
            ],
        ]);
    }
}