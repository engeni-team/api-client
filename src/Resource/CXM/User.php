<?php

namespace Engeni\ApiClient\Resource\CXM;

/*
 * Copyright 2022 Engeni
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class User extends BaseResource
{
    protected $resourceName = 'users';

    public function isCxmUser(): bool
    {
        return $this->isAdmin() || $this->isAnalyst() || $this->isPartner() || $this->isSales();
    }

    public function isAdmin(): bool
    {
        return $this->role_id == Role::ROLE_USER_ADMIN;
    }

    public function isPartner(): bool
    {
        return $this->role_id == Role::ROLE_USER_PARTNER;
    }

    public function isAnalyst(): bool
    {
        return $this->role_id == Role::ROLE_USER_ANALYST;
    }

    public function isSales(): bool
    {
        return $this->role_id == Role::ROLE_USER_SALES;
    }

    public function setCurrentAccount($params): bool
    {
        return self::do($this->getKey().'/set-current-account', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function setCurrentOrder($params): bool
    {
        return self::do($this->getKey().'/set-current-order', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function setCurrentPassword($params): bool
    {
        return self::do($this->getKey().'/set-password', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }

    public function register($params): bool
    {
        return self::do('/register', [
            'verb' => 'POST',
            'form_params' => $params,
        ]);
    }
}
