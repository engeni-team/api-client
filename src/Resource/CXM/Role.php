<?php

namespace Engeni\ApiClient\Resource\CXM;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Role extends BaseResource
{
    final public const ROLE_USER_ADMIN = 1;

    final public const ROLE_USER_PARTNER = 2;

    final public const ROLE_USER_ANALYST = 3;

    final public const ROLE_USER_SALES = 7;

    final public const ROLE_USER_GENERIC = 6;

    final public const ROLE_ACCOUNT_OWNER = 4;

    final public const ROLE_ACCOUNT_USER = 5;

    protected $resourceName = 'roles';
}
