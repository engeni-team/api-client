<?php

namespace Engeni\ApiClient\Resource\CXM;

/*
 * Copyright 2022 Engeni
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Auth extends BaseResource
{
    protected $resourceName = 'auth';

    public static function getByToken(string $bearerToken)
    {
        $instance = new static;

        $newClient = $instance->getClient();

        $newClient->addHeader('Authorization', 'Bearer '.$bearerToken);

        $query = $instance->newQuery()
            ->setClient($newClient)
            ->setPath($instance->getPath().'/user');

        $result = $query->first();

        return $result
            ? $instance->fill((array) $result)
            : null;
    }
}
