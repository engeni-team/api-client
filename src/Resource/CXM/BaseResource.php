<?php

namespace Engeni\ApiClient\Resource\CXM;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Engeni\ApiClient\Abstracts\ResourceAbstract;

class BaseResource extends ResourceAbstract
{
    protected $rootPath = 'cxm';

    public function __construct(array $params = [])
    {
        parent::__construct($params);
        $this->params = $params;
    }

    public static function getListByParams($params = [], $with = [])
    {
        $query = self::query();
        foreach ($params as $key => $value) {
            $query->where($key, $value);
        }
        foreach ($with as $value) {
            $query->with($value);
        }

        return $query->paginate();
    }

    public function paramsForRequest()
    {
        return ['timeout' => 10, 'query' => $this->params];
    }

    public function getData($resource)
    {
        return $this->getClient()->execute('GET', $this->getPath().$resource, $this->paramsForRequest());
    }
}
