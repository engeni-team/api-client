<?php

namespace Engeni\ApiClient\Resource\Syndi;

/*
 * Copyright 2023 Engeni LLC
 *
 * Licensed under the GNU GPLv3  (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.gnu.org/licenses/gpl-3.0.en.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Publication extends BaseResource
{
    protected $resourceName = 'publications';

    public function widget(array $params = [])
    {
        if (! $this->id) {
            throw new Exception('It is not possible to get a widget from an invalid Landing.');
        }

        $query = self::newQuery()->setPath($this->getPath().'/widget');

        if (isset($params['embed'])) {
            $query->with($params['embed']);
        }

        $resource = $this->getClient()->get($query);
        $contentData = $resource->getBody()->getContents();

        return json_decode($contentData, null, 512, JSON_THROW_ON_ERROR)->data;
    }

    /**
     * @throws \Exception
     */
    public function generate(array $attributes = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/generate');
        $resource = $this->getClient()->post($query, $attributes);
        $contentData = $resource->getBody()->getContents();

        return json_decode($contentData, null, 512, JSON_THROW_ON_ERROR)->data;
    }

    /**
     * @throws \Exception
     */
    public function regenerate(array $attributes = [])
    {
        $query = self::newQuery()->setPath($this->getPath().'/re-generate');
        $resource = $this->getClient()->post($query, $attributes);
        $contentData = $resource->getBody()->getContents();

        return json_decode($contentData, null, 512, JSON_THROW_ON_ERROR)->data;
    }

    public function publish()
    {
        return self::do($this->getKey().'/publish', [
            'verb' => 'POST',
        ]);
    }
}
