# Engeni API Client

A powerful PHP library for interacting with the Engeni API, providing an elegant and intuitive interface for managing Engeni's core resources.

## Table of Contents

- [Installation](#installation)
- [Configuration](#configuration)
- [Basic Usage](#basic-usage)
- [Core Resources](#core-resources)
- [Query Building](#query-building)
- [Resource Management](#resource-management)
- [Advanced Features](#advanced-features)

## Installation

Install the package via Composer:

```bash
composer require engeni/api-client
```

## Configuration

Initialize the client with your API credentials:

```php
use Engeni\ApiClient\Client;

$client = new Client([
    'base_uri' => 'https://api.your-domain.com',
    'debug' => true,
    'auth_token' => 'your-bearer-token'
]);

// Set as global client instance
$client->setAsGlobal();
```

## Basic Usage

### Making Simple Requests

```php
use Engeni\ApiClient\Query;

// Basic GET request
$query = (new Query)->setPath('/');
$response = (new Client)->get($query);
```

### Working with Resources

```php
use Engeni\ApiClient\Resource\Core\User\User;

// Fetch user by ID
$user = User::find($userId);

// Get user with related data
$user = User::with('profile')->find($userId);

// Update user settings
$user->saveSettings([
    'language' => 'en',
    'current_account_id' => 1,
    'current_location_id' => 1,
]);
```

## Core Resources

The library provides access to various core resources:

### Users
```php
use Engeni\ApiClient\Resource\Core\User\User;

// Get user by token
$user = User::getByToken($token, ['profile', 'language', 'accounts']);

// Paginate users
$users = User::paginate();
```

### Accounts
```php
use Engeni\ApiClient\Resource\Core\Account\Account;

// Find account
$account = Account::find($accountId);

// Query active accounts
$accounts = Account::where('active', '1')
    ->limit(2)
    ->get();
```

### Messages
```php
use Engeni\ApiClient\Resource\Core\Message;

$message = new Message();
$message->subscription = 'core';
$message->topic = 'api-client-test';
$message->account_id = 0;
$message->payload = [
    'from' => 'no-reply@example.com',
    'to' => 'recipient@example.com',
    'subject' => 'Test Message',
    'html_body' => '<p>Message content</p>',
];
$message->send();
```

## Query Building

The Query class provides a fluent interface for building API requests:

```php
use Engeni\ApiClient\Query;

$query = (new Query)
    ->select('id', 'name', 'email')
    ->where('active', true)
    ->whereIn('role', ['admin', 'manager'])
    ->with(['profile', 'permissions'])
    ->orderBy('created_at', 'DESC')
    ->limit(20);
```

### Available Query Methods

- `select(...$columns)`
- `where($field, $value)`
- `whereIn($field, array $values)`
- `limit($limit)`
- `with($relations)`
- `orderBy($field, $order = 'ASC')`
- `get()`
- `first()`
- `firstOrFail()`
- `find($id)`
- `pluck($column, $key = null)`

## Resource Management

Resources extend the `ResourceAbstract` class, providing consistent behavior across different types:

### Creating Resources
```php
$resource = Resource::create([
    'name' => 'New Resource',
    'type' => 'example'
]);
```

### Updating Resources
```php
$resource->fill([
    'name' => 'Updated Name'
]);
$resource->save();
```

### Deleting Resources
```php
$resource->delete();
// or
Resource::destroy($id);
```

## Advanced Features

### Parent Resources
```php
// Access child resources through parent
$childResources = ChildResource::fromParent($parentId)
    ->where('status', 'active')
    ->get();
```

### Pagination
```php
$paginator = Resource::paginate(20);
foreach ($paginator as $item) {
    // Process each item
}
```

### Custom Operations
```php
// Perform custom operations on resources
Resource::do('custom-action', $options, $parameters);
```

## Error Handling

The library throws exceptions for various error conditions:

- `ResourceNotFoundException`: When a requested resource is not found
- `ValidationException`: When resource validation fails
- `AuthenticationException`: When authentication fails
- `ApiException`: For general API errors

## Contributing

Contributions are welcome! Please feel free to submit a Pull Request.

## License

This project is licensed under the GNU GPLv3 License - see the LICENSE file for details.

## Support

For support, please contact Engeni LLC or open an issue in the repository.

---

For more detailed information and examples, please refer to the [official documentation](https://docs.engeni.com) or the source code.
